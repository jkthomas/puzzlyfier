// TODO: Add 'type' field for validation (like email, text, number, password)

export type Field<T> = {
  value: T;
  isError: boolean;
  errorMessage: string;
};
