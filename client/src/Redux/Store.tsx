import { AnyAction, configureStore } from '@reduxjs/toolkit';
import setsStatusReducer from '../Components/Redux/setsStatusSlice';

import storage from 'redux-persist/lib/storage';
import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import { useDispatch } from 'react-redux';
import thunk from 'redux-thunk';
import { Dispatch } from 'react';

const reducers = combineReducers({
  sets: setsStatusReducer,
});
const persistConfig = {
  key: 'root',
  storage,
};
const persistedReducer = persistReducer(persistConfig, reducers);

const store = configureStore({
  reducer: persistedReducer,
  devTools: process.env.NODE_ENV !== 'production',
  middleware: [thunk],
});
export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = (): Dispatch<AnyAction> =>
  useDispatch<AppDispatch>();

export default store;

// More info on https://redux.js.org/tutorials/essentials/part-2-app-structure#the-react-counter-component
