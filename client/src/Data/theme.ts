const theme = {
  bgColorSuccess: '#AED581',
  bgColorError: '#F48FB1',
  bgColorInfo: '#26A69A',

  borderColorSuccess: '#43A047',
  borderColorError: '#E53935',
  borderColorInfo: '#004D40',
};

export default theme;
