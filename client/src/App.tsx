import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { useSelector } from 'react-redux';
import Main from './Components/Main';
import { selectSetStatus } from './Components/Redux/setsStatusSlice';
import RiddlesSetContainer from './Components/Riddles/RiddlesSetContainer';
import { SetStateType } from './Components/Redux/RiddlesStateType';
import Login from './Sign/Login/LoginForm';
import Register from './Sign/Register/RegisterForm';

function App(): JSX.Element {
  const sets = useSelector(selectSetStatus);

  // TODO: Add authentication to dashboard and change default redirect from / to /login only for unathenticated
  // TODO: Change to PWA
  return (
    <Router>
      <Switch>
        <Redirect exact from="/" to="/login" />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/dashboard" component={Main} />
        {sets.map((set: SetStateType, index: number) => (
          <Route
            key={index}
            path={set.path}
            component={() => (
              <RiddlesSetContainer
                code={set.code}
                setId={set.id}
                title={set.title}
                riddles={set.riddles}
              />
            )}
          />
        ))}
      </Switch>
    </Router>
  );
}

export default App;
