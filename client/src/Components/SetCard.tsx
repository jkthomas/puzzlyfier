import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';
import { Box, TextField, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { changeSetStatusCode } from './Redux/setsStatusSlice';
import '../App.css';
import theme from '../Data/theme';
import { RootState, useAppDispatch } from '../Redux/Store';

interface SetCardInput {
  setNumber: number;
  setLink: string;
}

function SetCard({ setNumber, setLink }: SetCardInput): JSX.Element {
  const dispatch = useAppDispatch();
  const setStatus = useSelector((state: RootState) =>
    state.sets.find((setStatus) => setStatus.id === setNumber)
  );

  const handleSetCode = useCallback(
    (event) => {
      if (setStatus && !setStatus.isSolved) {
        dispatch(
          changeSetStatusCode({
            id: setNumber,
            currentCode: event.target.value,
          })
        );
      }
    },
    [dispatch, setStatus, setNumber]
  );

  return (
    <Box
      display="flex"
      justifyContent="space-around"
      alignItems="center"
      flexDirection="column"
      border={5}
      borderRadius={16}
      padding={2}
      bgcolor="white"
      borderColor={
        setStatus?.isSolved ? theme.borderColorSuccess : theme.borderColorError
      }
      boxShadow={5}
      margin={1}
    >
      <h1>Set {setNumber}</h1>
      <TextField
        id={`${setNumber}password`}
        label="Set code"
        variant="outlined"
        value={setStatus?.currentCode}
        onChange={handleSetCode}
        disabled={setStatus?.isSolved}
      />
      <Box marginTop={2}>
        <Link
          to={setLink}
          style={{ textDecoration: 'none' }}
          className={setStatus?.isSolved ? 'disabled-link' : ''}
        >
          <Button
            variant="contained"
            color="primary"
            disabled={setStatus?.isSolved}
          >
            Activate set
          </Button>
        </Link>
      </Box>
    </Box>
  );
}

export default SetCard;
