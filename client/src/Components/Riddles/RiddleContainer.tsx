import React, { useState, useCallback } from 'react';
import { Box, Button, TextField } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { changeRiddleAnswer } from '../Redux/setsStatusSlice';
import theme from '../../Data/theme';
import Content from './Content';
import Tip from './Tip';
import { RootState, useAppDispatch } from '../../Redux/Store';

interface RiddleContainerInput {
  id: number;
  type: string;
  content: string;
  tip1: {
    type: string;
    content: string;
  };
  tip2: {
    type: string;
    content: string;
  };
  setId: number;
}

function RiddleContainer({
  id,
  type,
  content,
  tip1,
  tip2,
  setId,
}: RiddleContainerInput): JSX.Element {
  const [isShowTip1, setIsShowTip1] = useState(false);
  const [isShowTip2, setIsShowTip2] = useState(false);
  const dispatch = useAppDispatch();
  const riddle = useSelector((state: RootState) =>
    state.sets
      .find((set) => set.id === setId)
      ?.riddles?.find((riddle) => riddle.id === id)
  );

  const handleSetAnswer = useCallback(
    (event) => {
      if (riddle && !riddle.isSolved) {
        dispatch(changeRiddleAnswer({ setId, id, answer: event.target.value }));
      }
    },
    [dispatch, id, setId, riddle]
  );

  const handleShowTip1 = useCallback(() => {
    setIsShowTip1(!isShowTip1);
  }, [isShowTip1]);
  const handleShowTip2 = useCallback(() => {
    setIsShowTip2(!isShowTip2);
  }, [isShowTip2]);

  return (
    <Box
      display="flex"
      justifyContent="space-around"
      alignItems="center"
      flexDirection="column"
      border={5}
      borderRadius={16}
      padding={5}
      bgcolor={riddle?.isSolved ? theme.bgColorSuccess : theme.bgColorError}
      borderColor={
        riddle?.isSolved ? theme.borderColorSuccess : theme.borderColorError
      }
      boxShadow={5}
      margin={5}
      width="70%"
    >
      <Content type={type} content={content} />

      {isShowTip1 && riddle && !riddle.isSolved && <Tip {...tip1} />}
      <Button
        variant="contained"
        color="primary"
        disabled={riddle?.isSolved}
        onClick={handleShowTip1}
      >
        {isShowTip1 ? 'Hide tip no 1' : 'Show tip no 1'}
      </Button>
      <br />
      {isShowTip2 && riddle && !riddle.isSolved && <Tip {...tip2} />}
      <Button
        variant="contained"
        color="primary"
        disabled={riddle?.isSolved}
        onClick={handleShowTip2}
      >
        {isShowTip2 ? 'Hide tip no 2' : 'Show tip no 2'}
      </Button>

      <Box marginTop={5}>
        <TextField
          // id={`${riddleSetNumber}password`}
          label="Riddle solution"
          variant="outlined"
          value={riddle?.answer}
          onChange={handleSetAnswer}
          disabled={riddle?.isSolved}
        />
      </Box>
    </Box>
  );
}

export default RiddleContainer;
