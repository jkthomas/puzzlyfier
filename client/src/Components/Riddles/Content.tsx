import React from 'react';
import { Box } from '@material-ui/core';
import theme from '../../Data/theme';

interface ContentInput {
  type: string;
  content: string;
}

function Content({ type, content }: ContentInput): JSX.Element | null {
  if (type === 'image') {
    return (
      <Box
        border={2}
        borderRadius={16}
        padding={5}
        bgcolor={theme.bgColorInfo}
        borderColor={theme.borderColorInfo}
        boxShadow={5}
        margin={5}
        width="75%"
      >
        <img
          style={{
            margin: '0 auto',
            maxWidth: '100%',
            height: 'auto',
            display: 'block',
          }}
          src={content}
          alt="missingImage"
        />
      </Box>
    );
  }
  if (type === 'text') {
    return (
      <Box
        border={2}
        borderRadius={16}
        padding={5}
        bgcolor={theme.bgColorInfo}
        borderColor={theme.borderColorInfo}
        boxShadow={5}
        margin={5}
        width="50%"
      >
        {content}
      </Box>
    );
  }
  return null;
}

export default Content;
