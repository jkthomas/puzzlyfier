import React from 'react';
import { Box, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import RiddleContainer from './RiddleContainer';
import theme from '../../Data/theme';
import { RootState } from '../../Redux/Store';
import { RiddleStateType } from '../Redux/RiddlesStateType';

interface RiddlesSetContainerInput {
  code: string;
  setId: number;
  title: string;
  riddles: RiddleStateType[];
}

function RiddlesSetContainer({
  code,
  setId,
  title,
  riddles,
}: RiddlesSetContainerInput): JSX.Element {
  // TODO: Use selector instead of props - riddle answer changes causes rerender!
  const riddlesSolved = useSelector((state: RootState) =>
    state.sets
      .find((set) => set.id === setId)
      ?.riddles?.every((status) => status.isSolved)
  );

  return (
    <Box
      display="flex"
      justifyContent="space-around"
      alignItems="center"
      flexDirection="column"
      border={5}
      borderRadius={16}
      padding={2}
      bgcolor={riddlesSolved ? theme.bgColorSuccess : theme.bgColorError}
      borderColor={
        riddlesSolved ? theme.borderColorSuccess : theme.borderColorError
      }
      boxShadow={5}
      margin={1}
    >
      <Link to="/" style={{ textDecoration: 'none' }}>
        <Button variant="contained" color="primary">
          Back
        </Button>
      </Link>

      <Box
        display="flex"
        justifyContent="space-around"
        alignItems="center"
        border={5}
        borderRadius={16}
        borderColor={theme.borderColorInfo}
        bgcolor={theme.bgColorInfo}
        padding={5}
      >
        {riddlesSolved ? (
          <h1>SET CODE: {code}</h1>
        ) : (
          <h1>Set code will show up when all riddles are solved</h1>
        )}
      </Box>

      <h1>{title}</h1>
      {riddles.map((riddle, index: number) => (
        <RiddleContainer key={index} {...riddle} setId={setId} />
      ))}

      <Box
        display="flex"
        justifyContent="space-around"
        alignItems="center"
        border={5}
        borderRadius={16}
        borderColor={theme.borderColorInfo}
        bgcolor={theme.bgColorInfo}
        padding={5}
      >
        {riddlesSolved ? (
          <h1>SET CODE: {code}</h1>
        ) : (
          <h1>Set code will show up when all riddles are solved</h1>
        )}
      </Box>

      <Link to="/" style={{ textDecoration: 'none' }}>
        <Button variant="contained" color="primary">
          Back
        </Button>
      </Link>
    </Box>
  );
}

export default RiddlesSetContainer;
