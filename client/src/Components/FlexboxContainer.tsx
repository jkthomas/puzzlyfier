import React from 'react';
import { Box } from '@material-ui/core';
import theme from '../Data/theme';

type Props = {
  children?: React.ReactNode;
};

function FlexboxContainer({ children }: Props): JSX.Element {
  return (
    <Box
      display="flex"
      justifyContent="space-around"
      alignItems="center"
      flexWrap="wrap"
      border={10}
      borderRadius={16}
      borderColor={theme.borderColorInfo}
      bgcolor={theme.bgColorInfo}
      padding={3}
      width="50%"
      height="50%"
    >
      {children}
    </Box>
  );
}

export default FlexboxContainer;
