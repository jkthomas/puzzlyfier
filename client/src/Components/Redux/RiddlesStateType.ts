export type RiddleStateType = {
  id: number;
  type: string;
  content: string;
  tip1: {
    type: string;
    content: string;
  };
  tip2: {
    type: string;
    content: string;
  };
  properAnswer: string;
  isSolved: boolean;
  answer: string;
};

export type SetStateType = {
  id: number;
  path: string;
  code: string;
  title: string;
  isSolved: boolean;
  currentCode: string;
  riddles: {
    id: number;
    type: string;
    content: string;
    tip1: {
      type: string;
      content: string;
    };
    tip2: {
      type: string;
      content: string;
    };
    properAnswer: string;
    isSolved: boolean;
    answer: string;
  }[];
};
