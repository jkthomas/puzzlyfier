import { SetStateType } from './RiddlesStateType';

export const initialState: Array<SetStateType> = [
  {
    id: 1,
    path: '/set1',
    code: '630972',
    title: 'Set 1',
    isSolved: false,
    currentCode: '',
    riddles: [
      {
        id: 1,
        type: 'image',
        content: 'https://i.imgur.com/aMSg4LT.png',
        tip1: {
          type: 'image',
          content:
            'https://i.pinimg.com/originals/bb/0d/67/bb0d67914d2802ba88fd1ee1e7129e8c.jpg',
        },
        tip2: { type: 'image', content: 'https://i.imgur.com/q1hzC1R.png' },
        properAnswer: 'hollow',
        isSolved: false,
        answer: '',
      },
      {
        id: 2,
        type: 'image',
        content: 'https://i.imgur.com/3gAQngb.png',
        tip1: {
          type: 'image',
          content: 'http://www.reactiongifs.com/r/2013/08/say-my-name.gif',
        },
        tip2: { type: 'text', content: 'What is his name?' },
        properAnswer: 'reece simpson',
        isSolved: false,
        answer: '',
      },
      {
        id: 3,
        type: 'image',
        content:
          'https://images.fineartamerica.com/images/artworkimages/mediumlarge/3/budapest-heroes-square-in-a-rainy-day-lorand-sipos.jpg',
        tip1: {
          type: 'image',
          content:
            'https://images.fineartamerica.com/images/artworkimages/mediumlarge/2/budapest-city-scape-artmarketjapan.jpg',
        },
        tip2: { type: 'text', content: 'Where is it?' },
        properAnswer: 'budapest',
        isSolved: false,
        answer: '',
      },
    ],
  },
  {
    id: 2,
    path: '/set2',
    code: '172056',
    title: 'Set 2',
    isSolved: false,
    currentCode: '',
    riddles: [
      {
        id: 1,
        type: 'text',
        content:
          '3 8 1 14 7 5 9 19 20 8 5 5 19 19 5 14 20 9 1 12 16 18 15 3 5 19 19 15 6 1 12 12 5 24 9 19 20 5 14 3 5',
        tip1: {
          type: 'image',
          content:
            'https://i.pinimg.com/originals/c6/16/23/c6162306b9b864e546e04a592ba29eaa.jpg',
        },
        tip2: { type: 'text', content: 'Who said that?' },
        properAnswer: 'spock',
        isSolved: false,
        answer: '',
      },
      {
        id: 2,
        type: 'image',
        content:
          'https://earth.esa.int/documents/257246/2705608/Armenia-2016-small-slider.jpg',
        tip1: { type: 'text', content: 'Is it Czech Republic?' },
        tip2: {
          type: 'text',
          content: 'This must be Asia...',
        },
        properAnswer: 'armenia',
        isSolved: false,
        answer: '',
      },
      {
        id: 3,
        type: 'text',
        content: '17z90p',
        tip1: { type: 'text', content: 'Who made that?' },
        tip2: {
          type: 'image',
          content:
            'https://i0.wp.com/pousta.com/wp-content/uploads/2018/11/Hide-the-Pain-Harold.jpg',
        },
        properAnswer: 'lg',
        isSolved: false,
        answer: '',
      },
    ],
  },
  {
    id: 3,
    path: '/set3',
    code: '876594',
    title: 'Set 3',
    isSolved: false,
    currentCode: '',
    riddles: [
      {
        id: 1,
        type: 'text',
        content: '269837',
        tip1: {
          type: 'image',
          content:
            'https://www.vedantu.com/seo/topicPages/a8c7ff58-0fac-469c-8e15-6aa0c7551c834305917455015967953.png',
        },
        tip2: {
          type: 'image',
          content: 'https://hugelolcdn.com/i/247013.jpg',
        },
        properAnswer: 'colorado',
        isSolved: false,
        answer: '',
      },
      {
        id: 2,
        type: 'image',
        content:
          'https://www.boxofficepro.com/wp-content/uploads/2020/07/nolan-479x600.png',
        tip1: {
          type: 'image',
          content:
            'https://i.kym-cdn.com/entries/icons/facebook/000/019/784/iconimadethis.jpg',
        },
        tip2: {
          type: 'image',
          content: 'http://www.filmconnection.com/FILM-STOCK/director.jpg',
        },
        properAnswer: 'christopher nolan',
        isSolved: false,
        answer: '',
      },
      {
        id: 3,
        type: 'image',
        content:
          'https://www.factinate.com/wp-content/uploads/2017/01/ForrestGump_BubbaGoesHome.jpg',
        tip1: {
          type: 'image',
          content:
            'https://images.theconversation.com/files/303322/original/file-20191124-74557-16zkcnl.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=496&fit=clip',
        },
        tip2: {
          type: 'image',
          content:
            'https://cottagelife.com/wp-content/uploads/2019/04/shutterstock_771740053-1-1200x801.jpg',
        },
        properAnswer: 'forrest gump',
        isSolved: false,
        answer: '',
      },
    ],
  },
  {
    id: 4,
    path: '/set4',
    code: '976055',
    title: 'Set 4',
    isSolved: false,
    currentCode: '',
    riddles: [
      {
        id: 1,
        type: 'image',
        content:
          'https://bucket.mn2s.com/wp-content/uploads/2018/10/13144327/backstreet-boys-title-pic-1024x489.jpg',
        tip1: { type: 'text', content: 'Crowd?' },
        tip2: {
          type: 'image',
          content:
            'https://www.woodandtools.com/2039-tm_large_default/simple-calculator.jpg',
        },
        properAnswer: '5',
        isSolved: false,
        answer: '',
      },
      {
        id: 2,
        type: 'image',
        content:
          'http://grandmaideas.com/wp-content/uploads/2016/06/word-of-the-day-new.jpg',
        tip1: {
          type: 'image',
          content:
            'https://www.unilad.co.uk/wp-content/uploads/2015/10/UNILAD-bad-smell4.jpg',
        },
        tip2: {
          type: 'image',
          content:
            'https://jasonbgraham.com/wp-content/uploads/2019/01/jasonbgraham-poop-icon.png',
        },
        properAnswer: 'shit',
        isSolved: false,
        answer: '',
      },
      {
        id: 3,
        type: 'image',
        content:
          'https://ascienceenthusiast.com/wp-content/uploads/2018/12/obi-wan-jesus-cover.jpg',
        tip1: {
          type: 'text',
          content: '-22.951815 -43.210442',
        },
        tip2: {
          type: 'image',
          content:
            'http://gregburdine.com/wp-content/uploads/2017/03/fish-and-bread.jpg',
        },
        properAnswer: 'jesus',
        isSolved: false,
        answer: '',
      },
    ],
  },
  {
    id: 5,
    path: '/set5',
    code: '726418',
    title: 'Set 5',
    isSolved: false,
    currentCode: '',
    riddles: [
      {
        id: 1,
        type: 'image',
        content:
          'http://fc02.deviantart.net/fs19/i/2007/241/e/e/Bender__s_Shiny_Metal_Ass_by_sircle.png',
        tip1: {
          type: 'image',
          content:
            'https://static.vecteezy.com/system/resources/previews/000/366/438/original/home-vector-icon.jpg',
        },
        tip2: { type: 'image', content: 'https://i.imgur.com/NmE6KFR.png' },
        properAnswer: 'rodriguez',
        isSolved: false,
        answer: '',
      },
      {
        id: 2,
        type: 'text',
        content: 'c e j a b',
        tip1: {
          type: 'image',
          content:
            'https://windows-cdn.softpedia.com/screenshots/Sound-of-the-Letters-and-Numbers_1.png',
        },
        tip2: {
          type: 'image',
          content:
            'https://media.istockphoto.com/vectors/earth-with-grid-lines-europe-and-africa-vector-id466584103?k=6&m=466584103&s=612x612&w=0&h=hogeJj07SgaFB3a4myMCftwxx-G5ufafGXz4dG5K2YU=',
        },
        properAnswer: 'equator',
        isSolved: false,
        answer: '',
      },
      {
        id: 3,
        type: 'image',
        content: 'https://naszywki24.pl/galerie/o/osiem-gwiazdek_2915.jpg',
        tip1: {
          type: 'image',
          content:
            'https://news.artnet.com/app/news-upload/2016/01/david-cover-up.jpeg',
        },
        tip2: {
          type: 'image',
          content:
            'https://ih1.redbubble.net/image.1165895370.9791/ur,mask_three_quarter,square,1000x1000.jpg',
        },
        properAnswer: 'censorship',
        isSolved: false,
        answer: '',
      },
    ],
  },
  {
    id: 6,
    path: '/set6',
    code: '525294',
    title: 'Set 6',
    isSolved: false,
    currentCode: '',
    riddles: [
      {
        id: 1,
        type: 'image',
        content:
          'http://geekyapar.com/wp-content/uploads/2015/09/Brooklyn-Nine-Nine-1.jpg',
        tip1: {
          type: 'image',
          content: 'https://assets.vg247.com/current/2015/06/hitman.jpg',
        },
        tip2: {
          type: 'image',
          content: 'https://i.ytimg.com/vi/X1W9pM7eesQ/maxresdefault.jpg',
        },
        properAnswer: 'barry',
        isSolved: false,
        answer: '',
      },
      {
        id: 2,
        type: 'image',
        content:
          'http://home.bt.com/images/in-moon-sam-rockwell-plays-an-astronaut-nearing-the-end-of-a-three-year-solitary-stint-in-a-space-station-who-suffers-a-personal-crisis-136384794231002601-131108100311.jpg',
        tip1: {
          type: 'image',
          content:
            'https://cdn.vox-cdn.com/thumbor/bdUG_nY1xb9-TOAHMYtzAIXTWFg=/0x0:2650x1766/1200x0/filters:focal(0x0:2650x1766):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/3765240/shutterstock_138668495.jpg',
        },
        tip2: {
          type: 'image',
          content:
            'http://www.popmythology.com/wp-content/uploads/2014/04/Two-Face-Hospital-Look-Right-Angry.jpeg',
        },
        properAnswer: 'Zaphod Beeblebrox',
        isSolved: false,
        answer: '',
      },
      {
        id: 3,
        type: 'image',
        content:
          'https://consequenceofsound.net/wp-content/uploads/2020/02/Bloodhound-Gang.jpg?quality=80',
        tip1: {
          type: 'image',
          content:
            'https://payload.cargocollective.com/1/18/604453/9795320/prt_210x160_1447944964_2x.png',
        },
        tip2: {
          type: 'image',
          content:
            'https://media.istockphoto.com/photos/the-breeding-of-monkey-picture-id530484442?k=6&m=530484442&s=612x612&w=0&h=kDlDnwxX3hajIEjarww3JE70nqU83cI5P1NJaW2JelQ=',
        },
        properAnswer: 'discovery channel',
        isSolved: false,
        answer: '',
      },
    ],
  },
  {
    id: 7,
    path: '/set7',
    code: '391965',
    title: 'Set 7',
    isSolved: false,
    currentCode: '',
    riddles: [
      {
        id: 1,
        type: 'image',
        content:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Red_and_blue_pill.jpg/1200px-Red_and_blue_pill.jpg',
        tip1: {
          type: 'image',
          content:
            'https://viewtech-contemposolution.netdna-ssl.com/wp-content/uploads/glossary_casting_form-300x225.jpg',
        },
        tip2: {
          type: 'image',
          content:
            'https://i.pinimg.com/originals/c8/48/7c/c8487cba74ca85b2b13edd244a6e61b1.png',
        },
        properAnswer: 'morpheus',
        isSolved: false,
        answer: '',
      },
      {
        id: 2,
        type: 'image',
        content:
          'https://media.wired.com/photos/59322e83edfced5820d0ed02/191:100/pass/snl-skits-ft.jpg',
        tip1: {
          type: 'image',
          content:
            'http://www.thedesignwork.com/wp-content/uploads/2011/08/script-writing.jpg',
        },
        tip2: {
          type: 'text',
          content:
            'I am a comedian, or as I like to call us, the last responders',
        },
        properAnswer: 'john mulaney',
        isSolved: false,
        answer: '',
      },
      {
        id: 3,
        type: 'text',
        content: "I'm in love",
        tip1: { type: 'text', content: 'But wait until three' },
        tip2: { type: 'text', content: 'Whisper my name and I yearn' },
        properAnswer: 'the cure',
        isSolved: false,
        answer: '',
      },
    ],
  },
];
