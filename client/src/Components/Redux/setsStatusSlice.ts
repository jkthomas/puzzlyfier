import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../../Redux/Store';
import { initialState } from './initialState';
import { SetStateType } from './RiddlesStateType';

const setsStatusSlice = createSlice({
  name: 'sets',
  initialState,
  reducers: {
    changeSetStatusCode: (state, action) => {
      const { id, currentCode } = action.payload;
      const existingSetStatus = state?.find((setStatus) => setStatus.id === id);
      if (existingSetStatus) {
        existingSetStatus.currentCode = currentCode;
        if (currentCode === existingSetStatus.code) {
          existingSetStatus.isSolved = true;
        }
      }
    },
    changeRiddleAnswer: (state, action) => {
      const { setId, id, answer } = action.payload;
      const existingRiddle = state
        ?.find((set) => set.id === setId)
        ?.riddles?.find((riddle) => riddle.id === id);
      if (existingRiddle) {
        existingRiddle.answer = answer;
        if (
          answer.toLowerCase() === existingRiddle.properAnswer.toLowerCase()
        ) {
          existingRiddle.isSolved = true;
        }
      }
    },
  },
});

export const {
  changeSetStatusCode,
  changeRiddleAnswer,
} = setsStatusSlice.actions;

export const selectSetStatus = (state: RootState): SetStateType[] => state.sets;
export const selectSetsStatusSolved = (state: RootState): boolean =>
  state.sets.every((status) => status.isSolved);

export default setsStatusSlice.reducer;
