import React from 'react';
import { Box } from '@material-ui/core';
import SetCard from './SetCard';
import { useSelector } from 'react-redux';
import {
  selectSetStatus,
  selectSetsStatusSolved,
} from './Redux/setsStatusSlice';
import theme from '../Data/theme';
import { SetStateType } from './Redux/RiddlesStateType';

function Main(): JSX.Element {
  const sets = useSelector(selectSetStatus);
  const areSetsSolved = useSelector(selectSetsStatusSolved);

  return (
    <>
      <Box
        display="flex"
        justifyContent="space-around"
        alignItems="center"
        border={10}
        borderRadius={16}
        borderColor={theme.borderColorInfo}
        bgcolor={theme.bgColorInfo}
      >
        <h1>Puzzlyfier</h1>
      </Box>
      <Box
        display="flex"
        justifyContent="space-around"
        alignItems="center"
        flexWrap="wrap"
        border={10}
        borderRadius={16}
        borderColor={theme.borderColorInfo}
        bgcolor={theme.bgColorInfo}
        padding={3}
      >
        {sets.map((set: SetStateType, index: number) => {
          return <SetCard key={index} setNumber={set.id} setLink={set.path} />;
        })}
      </Box>
      <Box
        display="flex"
        justifyContent="space-around"
        alignItems="center"
        border={10}
        borderRadius={16}
        borderColor={theme.borderColorInfo}
        bgcolor={theme.bgColorInfo}
        padding={10}
      >
        {areSetsSolved ? (
          <h1>PUZZLE SOLVED</h1>
        ) : (
          <h1>Puzzle sets solution will show upon completing all sets</h1>
        )}
      </Box>
    </>
  );
}

export default Main;
