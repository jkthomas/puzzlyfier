import React, { useCallback, useState } from 'react';
import { produce } from 'immer';
import FlexboxContainer from '../../Components/FlexboxContainer';
import { Box, Button, TextField } from '@material-ui/core';
import { Link } from 'react-router-dom';
import {
  RegisterFormFields,
  initialLoginFormFields,
} from './RegisterFormFields';

function Register(): JSX.Element {
  const [values, setValues] = useState<RegisterFormFields>(
    initialLoginFormFields
  );

  // TODO: Create handler for produce setvalues for objects (hook probably), to not repeat it all the time
  const handleEmailChange = useCallback((event) => {
    setValues(
      produce((draft: RegisterFormFields) => {
        draft.email.value = event.target.value;
      })
    );
  }, []);

  const handleLoginChange = useCallback((event) => {
    setValues(
      produce((draft: RegisterFormFields) => {
        draft.login.value = event.target.value;
      })
    );
  }, []);

  const handlePasswordChange = useCallback((event) => {
    setValues(
      produce((draft: RegisterFormFields) => {
        draft.password.value = event.target.value;
      })
    );
  }, []);

  const handleRepeatPasswordChange = useCallback((event) => {
    setValues(
      produce((draft: RegisterFormFields) => {
        draft.repeatPassword.value = event.target.value;
      })
    );
  }, []);

  const handleRegister = useCallback(
    (input) => {
      console.log(values);
    },
    [values]
  );

  const isRegisterDisabled =
    values.email.value === '' ||
    values.login.value === '' ||
    values.password.value === '' ||
    values.repeatPassword.value === '';

  return (
    <FlexboxContainer>
      <Box
        display="flex"
        justifyContent="space-around"
        alignItems="center"
        flexDirection="column"
        height="70%"
      >
        <TextField
          label="Email"
          value={values.email.value}
          onChange={handleEmailChange}
        />
        <TextField
          label="Login"
          value={values.login.value}
          onChange={handleLoginChange}
        />
        <TextField
          label="Password"
          type="password"
          value={values.password.value}
          onChange={handlePasswordChange}
        />
        <TextField
          label="Repeat Password"
          type="password"
          value={values.repeatPassword.value}
          onChange={handleRepeatPasswordChange}
        />
        <Button
          size="large"
          variant="contained"
          color="primary"
          disabled={isRegisterDisabled}
          onClick={handleRegister}
        >
          Register
        </Button>
        <Link to="/login" style={{ textDecoration: 'none' }}>
          <Button size="small" variant="contained" color="primary">
            Back to login
          </Button>
        </Link>
      </Box>
    </FlexboxContainer>
  );
}

export default Register;
