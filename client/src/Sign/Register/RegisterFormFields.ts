import { Field } from '../../Validation/Field';

export type RegisterFormFields = {
  email: Field<string>;
  login: Field<string>;
  password: Field<string>;
  repeatPassword: Field<string>;
};

export const initialLoginFormFields: RegisterFormFields = {
  email: {
    value: '',
    isError: false,
    errorMessage: '',
  },
  login: {
    value: '',
    isError: false,
    errorMessage: '',
  },
  password: {
    value: '',
    isError: false,
    errorMessage: '',
  },
  repeatPassword: {
    value: '',
    isError: false,
    errorMessage: '',
  },
};
