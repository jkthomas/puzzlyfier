import { Field } from '../../Validation/Field';

export type LoginFormFields = {
  login: Field<string>;
  password: Field<string>;
};

export const initialLoginFormFields: LoginFormFields = {
  login: {
    value: '',
    isError: false,
    errorMessage: '',
  },
  password: {
    value: '',
    isError: false,
    errorMessage: '',
  },
};
