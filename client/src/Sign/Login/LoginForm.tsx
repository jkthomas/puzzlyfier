import React, { useCallback, useState } from 'react';
import { produce } from 'immer';
import FlexboxContainer from '../../Components/FlexboxContainer';
import { Box, Button, TextField } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { LoginFormFields, initialLoginFormFields } from './LoginFormFields';

function Login(): JSX.Element {
  const [values, setValues] = useState<LoginFormFields>(initialLoginFormFields);
  const handleLoginChange = useCallback((event) => {
    setValues(
      produce((draft: LoginFormFields) => {
        draft.login.value = event.target.value;
      })
    );
  }, []);

  const handlePasswordChange = useCallback((event) => {
    setValues(
      produce((draft: LoginFormFields) => {
        draft.password.value = event.target.value;
      })
    );
  }, []);

  const handleLogin = useCallback(
    (input) => {
      console.log(values);
    },
    [values]
  );

  const handleValidate = useCallback((e) => {
    console.log('VALIDATE');
    // TODO: Add validation
  }, []);

  return (
    <FlexboxContainer>
      <Box
        display="flex"
        justifyContent="space-around"
        alignItems="center"
        flexDirection="column"
        height="60%"
      >
        <TextField
          label="Login"
          value={values.login.value}
          onChange={handleLoginChange}
          onBlur={handleValidate} // TODO: Think about blur vs submit validation
        />
        <TextField
          label="Password"
          type="password"
          value={values.password.value}
          onChange={handlePasswordChange}
        />
        <Button
          size="large"
          variant="contained"
          color="primary"
          disabled={values.login.value === '' || values.password.value === ''}
          onClick={handleLogin}
        >
          Login
        </Button>
        <Link to="/register" style={{ textDecoration: 'none' }}>
          <Button size="small" variant="contained" color="primary">
            Register
          </Button>
        </Link>
      </Box>
    </FlexboxContainer>
  );
}

export default Login;
